Voici le brief :

    intégrer la page de manière sémantique, en HTML5 suivant les principes du RWD
    réaliser un script de parallaxe  (en utilisant l'état de l'art : Pattern POO et/ou plugin jQuery) et l'appliquer à la première section (le logo, le fond et le contenu texte qui suit défilent à des vitesses différentes). Appliquer cet effet de parallaxe sur une autre des sections de la page pour tester que le plugin fonctionne sur plusieurs instances.
    Le menu reste fixe lors du scroll
    réaliser ou implémenter un script pour le bloc publicitaire de la section 2 (300x600) qui reste fixe par rapport au viewport lors du scroll tout le long de la section puis s'arrête au bas de la colonne 

Il n'y aucune obligation à tout intégrer, je préfère un projet livré "propre" à un projet mal "fini" (par exemple seule une partie de la page avec toutes ses fonctionnalités est livrée, plutôt que toute la page avec ses styles mais aucune fonctionnalité). 
Il n'y a aucune obligation en matière de librairie, l'idée est avant tout de produire un code réutilisable, configurable en fonction de la situation,

Vous pouvez également mettre en place un workflow basique sur grunt ou webpack pour générer votre code optimisé mais je souhaite recevoir dans les livrables les codes sources non optimisés afin de les analyser.

Notez bien que la maquette ne peut pas le représenter par nature mais il s'agit bien du viewport qui est représenté et donc le site occupe bien toute la largeur de ce dernier.

Les éléments en pièces jointes ci-dessous sont :
- La maquette source en sketch (disponible uniquement pour mac sur sketchapp)
- Un export des assets (images, textes ... ) pour les candidats qui ne travaillent pas dans un environnement mac.

Je reste disponible pour toute question concernant le test.

Bonne réception,

